package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
)

func main() {
	router := mux.NewRouter()

	headers := handlers.AllowedHeaders([]string{"X-Requested-With", "Content-Type", "Authorization"})
	methods := handlers.AllowedMethods([]string{"GET", "POST", "PUT", "DELETE"})
	origins := handlers.AllowedOrigins([]string{"*"})

	// Handle All Issues
	router.HandleFunc("/issues", func(w http.ResponseWriter, r *http.Request) {
		requestBoddy, err := json.Marshal(map[string]string{
			"query": `{
				repository(owner:"facebook", name:"react") {
					issues(first:15, states: OPEN){
						nodes {
							number
							title
							author{
								login
							}
							comments(last: 5){
							nodes{
								author{login}
								body
							}
							}
						}
					}
				}
			}`,
		})

		if err != nil {
			log.Fatalln(err)
		}

		timeout := time.Duration(5 * time.Second)
		client := http.Client{
			Timeout: timeout,
		}

		request, err := http.NewRequest("POST", "https://api.github.com/graphql", bytes.NewBuffer(requestBoddy))
		request.Header.Set("Authorization", "bearer 111cf150f7002fb2048fedc9bb059d480644c3d4")

		if err != nil {
			log.Fatalln(err)
		}

		resp, err := client.Do(request)
		if err != nil {
			log.Fatalln(err)
		}

		defer resp.Body.Close()

		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Fatalln(err)
		}

		w.Write(body)
	}).Methods("GET")

	// Handle Specific Issues
	router.HandleFunc("/issues/{number}", func(w http.ResponseWriter, r *http.Request) {
		params := mux.Vars(r)
		number, err := strconv.ParseInt(params["number"], 10, 64)

		requestBoddy, err := json.Marshal(map[string]string{
			"query": fmt.Sprintf(`
			{
				repository(owner: "facebook", name: "react") {
				issue(number:  %d) {
					number
					title
					author {
						login
					  }
					body
					comments(last: 10) {
					nodes {
						id
						author {
							avatarUrl
							login
						}
						body
					}
					}
				}
				}
			}
		`, number),
		})

		if err != nil {
			log.Fatalln(err)
		}

		timeout := time.Duration(5 * time.Second)
		client := http.Client{
			Timeout: timeout,
		}

		request, err := http.NewRequest("POST", "https://api.github.com/graphql", bytes.NewBuffer(requestBoddy))
		request.Header.Set("Authorization", "bearer 111cf150f7002fb2048fedc9bb059d480644c3d4")

		if err != nil {
			log.Fatalln(err)
		}

		resp, err := client.Do(request)
		if err != nil {
			log.Fatalln(err)
		}

		defer resp.Body.Close()

		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Fatalln(err)
		}

		w.Write(body)
	}).Methods("GET")

	fmt.Println("Running on http://localhost:6060")
	http.ListenAndServe(":8080", handlers.CORS(headers, methods, origins)(router))

}
