import React, { Component } from 'react';
import ListIssue from "./containers/ListIssue";
import DetailIssue from "./containers/DetailIssue";
import { Route, Switch, BrowserRouter as Router } from 'react-router-dom';


class App extends Component {
  // fetchIssues() {
  //   axios({
  //     method: "GET",
  //     url: "http://localhost:6060/issues/285"
  //   })
  //   .then(response => {
  //     console.log(response);

  //   })
  // }

  render() {
    // this.fetchIssues()
    return (
      <Router>
        <div className="container-content">
          <Switch>
            <Route exact path="/" component={ListIssue} />
            <Route exact path="/detail" component={DetailIssue} />
          </Switch>
        </div>
      </Router>
    );
  }
}

export default App;