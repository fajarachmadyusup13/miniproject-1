import React, { Component } from 'react';
import { Comment } from 'semantic-ui-react';
class SingleComment extends Component {
    render() {
        const comment = this.props.comment;
        const { author, body } = comment;
        
        return (
            <div>
                <Comment>
                        <Comment.Avatar src={author.avatarUrl} />
                        <Comment.Content>
                            <Comment.Author as='a'>{author.login}</Comment.Author>
                            <Comment.Text>{body}</Comment.Text>
                        </Comment.Content>
                    </Comment>
            </div>
        );
    }
}

export default SingleComment;