import React, { Component } from 'react';
import { Card } from 'semantic-ui-react';

class SingleIssue extends Component {
    render() {
        const issue = this.props.issue;
        const title = issue.title;
        const number = issue.number;
        const author = issue.author.login;
        return (
            <Card className="content">
                <Card.Content>
                    <Card.Header>
                        <a onClick={() => this.props.history.push(`/detail?number=${number}`)}>
                            {title}
                        </a>
                    </Card.Header>
                    <Card.Meta>
                        <span className='date'>#{number} created by {author}</span>
                    </Card.Meta>
                </Card.Content>
            </Card>
        );
    }
}

export default SingleIssue;