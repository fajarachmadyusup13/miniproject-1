import React, { Component } from 'react';
import qs from 'querystring';
import axios from "axios";
import SingleComment from "../components/SingleComment";
import { Card, Comment, Header } from 'semantic-ui-react';

class DetailIssue extends Component {
    constructor(props) {
        super(props);
        this.state = {
            issue: {},
            title: '',
            body: '',
            author: '',
            number: 0,
            comments: []
        }
    }

    fetchDetailIssue() {
        const search = qs.decode(this.props.location.search.slice(1))
        axios({
            method: "GET",
            url: `http://localhost:8080/issues/${search.number}`
        })
            .then(response => {
                const repository = response.data.data.repository

                const issue = repository.issue
                const title = issue.title;
                const author = issue.author.login;
                const body = issue.body;
                const number = issue.number;
                const comments = issue.comments.nodes;

                this.setState({
                    issue,
                    title,
                    body,
                    number,
                    author,
                    comments
                })

            })
    }

    componentDidMount() {
        this.fetchDetailIssue()
    }

    render() {
        // console.log(this.state.issue);
        // const issueRes = this.state.issue;
        
        // const author = (issueRes.author && issueRes.author.login);
        // const comments = (issueRes.comments && issueRes.comments.nodes)
        

        return (
            <div>
                <Card className="content">
                    <Card.Content >
                        <Card.Header>
                            {this.state.title}
                        </Card.Header>
                        <Card.Meta>
                            <span className='date'>#{this.state.number} created by {this.state.author}</span>
                        </Card.Meta>
                    </Card.Content>
                    <Card.Content>
                        <Card.Description>
                            {this.state.body}
                        </Card.Description>
                    </Card.Content>
                </Card>

                <Comment.Group>
                    <Header as='h3' dividing>
                        Comments
                    </Header>
                    {
                        this.state.comments.map(comment => {
                            return (<SingleComment comment={comment} key={comment.id}/>)
                        })
                    }
                </Comment.Group>
            </div>

        );
    }
}

export default DetailIssue;