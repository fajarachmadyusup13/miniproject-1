import React, { Component } from 'react';
import axios from "axios";
import SingleIssue from "../components/SingleIssue";


class ListIssue extends Component {

    constructor(props) {
        super(props);
        this.state = {
            issues: []
        }
    }


    fetchIssues() {
        axios({
            method: "GET",
            url: "http://localhost:8080/issues"
        })
            .then(response => {
                
                const repository = response.data.data.repository
                const issues = repository.issues.nodes
                this.setState({
                    issues
                })

            })
    }

    componentDidMount() {
        this.fetchIssues()
    }

    render() {
        return (
            <div>
                {this.state.issues.map(issue => {
                    return <SingleIssue key={issue.number} issue={issue} history={this.props.history} />
                })}
            </div>
        );
    }
}

export default ListIssue;